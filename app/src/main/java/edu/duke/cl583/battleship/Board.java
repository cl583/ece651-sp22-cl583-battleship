package edu.duke.cl583.battleship;

import java.io.IOException;

public interface Board<T> {
  public int getWidth();
  public int getHeight();
  public String tryAddShip(Ship<T> toAdd);
  public T whatIsAtForSelf(Coordinate where);
  public Ship<T> fireAt(Coordinate c) throws IOException;
  public T whatIsAtForEnemy(Coordinate where);
  public boolean isAllSunk();
  public String doSonar(Coordinate c, Board<T> enemyBoard);
}
