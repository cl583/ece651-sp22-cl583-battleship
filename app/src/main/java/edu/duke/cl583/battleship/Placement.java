package edu.duke.cl583.battleship;

public class Placement {
  private final Coordinate where;
  private final char orientation;

  public Placement(final Coordinate where, final char orientation) {
    this.where = where;
    this.orientation = Character.toUpperCase(orientation);
  }

  public Placement(String descr) {
    if (descr.length() != 3) {
      throw new IllegalArgumentException("Error: Input string expect to be legnth 3 but get " + descr.length());
    }

    this.where = new Coordinate(descr.substring(0, descr.length() - 1));
    char o_symbol = descr.toUpperCase().charAt(descr.length() - 1);
    if (o_symbol != 'H'
        && o_symbol != 'V'
        && o_symbol != 'U'
        && o_symbol != 'D'
        && o_symbol != 'L'
        && o_symbol != 'R') {
      throw new IllegalArgumentException("Error: Invalid Orientation expected H or V, but get " + o_symbol);
    }

    this.orientation = o_symbol;
  }

  public Coordinate getCoordinate() {
    return where;
  }

  public char getOrientation() {
    return orientation;
  }

  @Override
  public String toString() {
    return where.toString() + ", " + orientation;
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Placement p = (Placement) o;
      return (where.equals(p.where)) && (orientation == p.orientation);
    }

    return false;
  }
}
