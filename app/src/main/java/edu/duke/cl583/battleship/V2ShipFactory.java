package edu.duke.cl583.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Function;

public class V2ShipFactory extends V1ShipFactory {
  /**
   * Constructor for V2ShipFactory
   */
  protected Ship<Character> createTriangleShip(Placement where, int w, int h, char letter, String name) {
    Ship<Character> ship = new TriangleShip<Character>(name, where.getCoordinate(), w, h, letter, '*'); // create a up
                                                                                                        // triangle
    return ship;
  }
  /*
   * function to creaate weirdship
   */
  protected Ship<Character> createWeirdShip(Placement where, int w, int h, char letter, String name) {
    Ship<Character> ship = new WeirdShip<Character>(name, where.getCoordinate(), w, h, letter, '*');
    return ship;
  }

  /**
   * Function to make BattleShip
   */
  @Override
  public Ship<Character> makeBattleship(Placement where) {
    Ship<Character> ship = createTriangleShip(where, 3, 2, 'b', "Battleship");
    makeRotation(where, ship, 3, 2);
    return ship;
  }

  /**
   * function to make carrier
   */
  @Override
  public Ship<Character> makeCarrier(Placement where) {
    Ship<Character> ship = createWeirdShip(where, 2, 5, 'c', "Carrier");
    makeRotation(where, ship, 2, 5);
    return ship;
  }

  protected void makeRotation(Placement where, Ship<Character> ship, int w, int h) {
    char[] orientArr = new char[3];
    orientArr[0] = 'R';
    orientArr[1] = 'D';
    orientArr[2] = 'L';

    if (where.getOrientation() == 'U') {
      return;
    }

    for (int i = 0; i < orientArr.length; i++) {
      if (orientArr[i] == where.getOrientation()) {
        rotateClockWise90Deg(where.getCoordinate(), ship, w, h);
        return;
      }
      rotateClockWise90Deg(where.getCoordinate(), ship, w, h);
    }
  }

  /**
   * Function to rotate ship
   */
  protected void rotateClockWise90Deg(Coordinate upperLeft, Ship<Character> ship, int w, int h) {
    // create a 2D matrix with
    Character[][] matrix = create2DMatrix(upperLeft, ship, w, h);
    // do rotation
    rotateMatrix90Deg(matrix);
    // algin rotate image with
    alignCoordinateWUpperLeft(matrix);
    addNewCoordinate(ship, upperLeft, matrix);
  }

  /**
   * Function to add new Coordiante
   */
  protected void addNewCoordinate(Ship<Character> ship, Coordinate upperLeft, Character matrix[][]) {
    ship.destroyCoordinate();

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] == 'O') {
          int row = upperLeft.getRow() + i;
          int col = upperLeft.getColumn() + j;
          Coordinate c = new Coordinate(row, col);
          ship.addCoordinate(c);
        }
      }
    }
  }

  /**
   * function to coordinate lower 2d matrix
   */
  protected void alignCoordinateWUpperLeft(Character[][] matrix) {
    int err = 0;
    int r = matrix.length - 1;
    int c = matrix[0].length - 1;
    while ((err = checkOffUpper(matrix)) != 0) {
      if (err == -1) {
        for (int i = 0; i < matrix[0].length; i++) {
          matrix[r - 2][i] = matrix[r - 1][i];
          matrix[r - 1][i] = 'E';
          matrix[r - 1][i] = matrix[r][i];
          matrix[r][i] = 'E';
        }

        r -= 1;
      }

      if (err == -2) {
        for (int j = 0; j < matrix.length; j++) {
          matrix[j][c - 2] = matrix[j][c - 1];
          matrix[j][c - 1] = 'E';
          matrix[j][c - 1] = matrix[j][c];
          matrix[j][c] = 'E';
        }

        c -= 1;
      }
    }
  }

  protected int checkOffUpper(Character[][] matrix) {
    int offCount = 0;
    for (int i = 0; i < matrix[0].length; i++) {
      if (matrix[0][i] == 'E') {
        offCount += 1;
      }
    }

    if (offCount == matrix[0].length) {
      return -1;
    }

    offCount = 0;
    for (int i = 0; i < matrix.length; i++) {
      if (matrix[i][0] == 'E') {
        offCount += 1;
      }
    }

    if (offCount == matrix.length) {
      return -2;
    }

    return 0;

  }

  protected Character[][] create2DMatrix(Coordinate upperLeft, Ship<Character> ship, int width, int height) {
    int matrixDim = Math.max(width, height);
    Iterable<Coordinate> coords = ship.getCoordinates();
    Character[][] matrix = new Character[matrixDim][matrixDim];
    for (Coordinate coord : coords) {
      int row = coord.getRow() - upperLeft.getRow();
      int column = coord.getColumn() - upperLeft.getColumn();
      matrix[row][column] = 'O';
    }

    for (int i = 0; i < matrixDim; i++) {
      for (int j = 0; j < matrixDim; j++) {
        if (matrix[i][j] == null) {
          matrix[i][j] = 'E';
        }
      }
    }

    return matrix;
  }

  protected void rotateMatrix90Deg(Character[][] matrix) {
    int n = matrix.length;
    for (int i = 0; i < (n + 1) / 2; i++) {
      for (int j = 0; j < n / 2; j++) {
        Character temp = matrix[n - 1 - j][i];
        matrix[n - 1 - j][i] = matrix[n - 1 - i][n - j - 1];
        matrix[n - 1 - i][n - j - 1] = matrix[j][n - 1 - i];
        matrix[j][n - 1 - i] = matrix[i][j];
        matrix[i][j] = temp;
      }
    }
  }
}
