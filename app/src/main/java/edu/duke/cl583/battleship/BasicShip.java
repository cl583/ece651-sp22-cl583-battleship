package edu.duke.cl583.battleship;

import java.util.HashMap;
import java.util.Map;

public abstract class BasicShip<T> implements Ship<T> {
  protected HashMap<Coordinate, Boolean> myPieces;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  private final String name;

  /**
   * Constructor for BasicShip
   * 
   * @param where         is the position to place ship
   * @param myDisplayInfo an object to display information
   */
  public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, String name) {
    this.myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate c : where) {
      myPieces.put(c, false);
    }

    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.name = name;
  }

  /**
   * Check whether the coordinate is occupied by this ship
   * 
   * @param where is the postion to see occupy
   * @return true or false
   */
  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    return myPieces.containsKey(where);
  }

  @Override
  public boolean isSunk() {
    int hit = 0;
    for (Map.Entry<Coordinate, Boolean> s : myPieces.entrySet()) {
      if (this.wasHitAt(s.getKey())) {
        hit += 1;
      }
    }

    return (hit == myPieces.size());
  }

  @Override
  public void recordHitAt(Coordinate where) throws IllegalArgumentException {
    this.checkCoordinateInThisShip(where);
    myPieces.put(where, true);
  }

  @Override
  public boolean wasHitAt(Coordinate where) throws IllegalArgumentException {
    this.checkCoordinateInThisShip(where);
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip) {
    if (myShip == true) {
      if (wasHitAt(where) == true) {
        return myDisplayInfo.getInfo(where, true);
      }
      return myDisplayInfo.getInfo(where, false);
    }

    if (wasHitAt(where) == true) {
      return enemyDisplayInfo.getInfo(where, true);
    }

    return enemyDisplayInfo.getInfo(where, false);
  }

  @Override
  public Iterable<Coordinate> getCoordinates() {
    return myPieces.keySet();
  }

  protected void checkCoordinateInThisShip(Coordinate c) {
    if (myPieces.containsKey(c) == false) {
      throw new IllegalArgumentException("Coordinate not in ship");
    }
  }

  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void destroyCoordinate() {
    myPieces.clear();
  }

  @Override
  public void addCoordinate(Coordinate c) {
    myPieces.put(c, false);
  }

}
