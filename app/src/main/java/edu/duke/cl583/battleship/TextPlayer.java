package edu.duke.cl583.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
  final Board<Character> theBoard;
  final BoardTextView view;
  final BufferedReader inputReader;
  final PrintStream out;
  final AbstractShipFactory<Character> shipFactory;
  final String name;
  final ArrayList<String> shipsToPlace;
  final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  int sonarMove;
  int moveCount;

  /**
   * Constructor fo TextPlayer
   * 
   * @param name:   the name for player
   * @param board:  board for this player
   * @param input   stream
   * @param output: output stream
   */
  public TextPlayer(String name, Board<Character> board, BufferedReader input, PrintStream out,

      AbstractShipFactory<Character> shipFactory) {
    this.name = name;
    this.theBoard = board;
    this.view = new BoardTextView(theBoard);
    this.inputReader = input;
    this.out = out;
    this.shipFactory = shipFactory;
    this.shipsToPlace = new ArrayList<String>();
    this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
    this.sonarMove = 3;
    this.moveCount = 3;
  }

  /**
   * Read Placement from user with the prompt
   */

  public Placement readPlacement(String prompt) throws IllegalArgumentException, EOFException, IOException {
    out.println(prompt);
    boolean invalid = true;
    Placement p = null;
    while (invalid) {
      try {
        String s = inputReader.readLine();
        if (s == null) {
          throw new EOFException();
        }

        p = new Placement(s);
        invalid = false;
      }

      catch (IllegalArgumentException e) {
        out.println("Invalid Placement, please re-enter");
      }
    }
    // Placement p = new Placement("A0H");
    return p;
  }

  /**
   * Read Coordinate from user with the prompt
   */
  public Coordinate readCoordinate(String prompt) throws IOException, IllegalArgumentException {
    out.println(prompt);
    String s = inputReader.readLine();
    if (s == null) {
      throw new EOFException();
    }

    return new Coordinate(s);
  }

  /**
   * do placement in the board read from user
   */
  public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) {

    try {
      Placement c = readPlacement("Player " + this.name + " where would you like to put your " + shipName + "?");

      Ship<Character> s = createFn.apply(c);
      while (theBoard.tryAddShip(s) != null) {
        out.println(theBoard.tryAddShip(s));
        c = readPlacement("re-enter a non collision or not our of bound placement");
        s = createFn.apply(c);
      }
    }

    catch (IOException e) {
      out.println("Undefined behavior, please reboot game");
    }
    out.print(view.displayMyOwnBoard());

  }

  /**
   * Player do the placement of their ship at the begining of the game
   **/
  public void doPlacementPhase() {
    out.print(view.displayMyOwnBoard());
    String prompt = "Player " + name
        + ": you are going to place the following ships (which are all rectangular). For each ship, type the coordinate of the upper left side of the ship, followed by either H (for horizontal) or V (for vertical).  For example M4H would place a ship horizontally starting at M4 and going to the right.  You have\n2 \"Submarines\" ships that are 1x2\n3 \"Destroyers\" that are 1x3\n3 \"Battleships\" that are 1x4\n2 \"Carriers\" that are 1x6\n";
    out.print(prompt);
    setupShipCreationMap();
    setupShipCreationList();

    for (String ship : shipsToPlace) {
      doOnePlacement(ship, shipCreationFns.get(ship));
    }
  }

  /**
   * create a hashmap that map string to corresponding function to make ship
   **/
  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("BattleShip", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
  }

  // TODO: change the ship to right amount
  protected void setupShipCreationList() {
    shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(3, "BattleShip"));
    shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
  }

  protected void dofireAt(Board<Character> enemyBoard) {
    boolean valid = false;
    while (valid != true) {
      try {
        Coordinate c = readCoordinate("player " + name + " choose coordinate to attack");
        Ship<Character> attackShip = enemyBoard.fireAt(c);
        if (attackShip != null) {
          out.println("You hit " + attackShip.getName() + "!");
        }

        else {
          out.println("You missed!");
        }
      }

      catch (IOException e) {
        out.println("Invalid Coordinate re-enter!");
        continue;
      }

      catch (IllegalArgumentException e) {
        out.println("Invalid Coordinate re-enter!");
        continue;
      }

      valid = true;
    }
  }

  protected void doSonar(Board<Character> enemyBoard) {
    out.println("please input coordinate to search");
    boolean invalid = true;
    while (invalid) {
      try {
        String loc = inputReader.readLine();
        Coordinate c = new Coordinate(loc);
        out.println(theBoard.doSonar(c, enemyBoard));
        invalid = false;
      }

      catch (IOException e) {
        out.println("Invalid, re-enter");
      }
    }
    
  }

  protected String askDoAction() throws IOException {
    out.println("Possble Action for " + name);
    out.println(" F Fire at a square");
    out.println(" M Move a ship to another square(" + moveCount + "remaining)");
    out.println(" S Sonar scan (" + sonarMove + "remaining)");
    String s = inputReader.readLine();
    return s;

  }

  public void playOneTurn(Board<Character> enemyBoard, String enemyName) {
    BoardTextView enemyView = new BoardTextView(enemyBoard);
    out.println(view.displayMyBoardWithEnemyNextToIt(enemyView, " Your Ocean", enemyName + "'s Ocean"));
    

    boolean invalid = true;
    while (invalid) {
      try {
        String action = askDoAction();
        if (action.equals("F")) {
          dofireAt(enemyBoard);
          invalid = false;
        }

        else if (action.equals("M")) {
          out.println("move");
          invalid = false;
        }

        else if (action.equals("S")) {
          sonarMove -= 1;
          doSonar(enemyBoard);
          invalid = false;
        }

        else {
          out.println("please input either F, M, or S");
        }
      }

      catch (IOException e) {
        out.println("invalid, re-enter");
      }
     }
  }

  public boolean isLose() {
    if (theBoard.isAllSunk() == true) {
      return true;
    }

    return false;
  }

}
