package edu.duke.cl583.battleship;

/**
 * This class indicate position on and 2D plain baord
 * @author: Ian Liu
 * @version: 1.0
 * @since: 2021/01/23
 */
public class Coordinate {
  private final int row;
  private final int column;
  /**
   * Constructor for coordinate
   * @param r the row in coordinate
   * @param c the column in coordinate
   */
  public Coordinate(int r, int c) {
    this.row = r;
    this.column = c;
  }

  /**
   * @param descr is the input string to represnet the coordinate in 2D board
   * it's length cannot be longer than 2, also it must follow the format
   * of [alphabet a - z][number 0 - 9](row/col)
   */
  public Coordinate(String descr) {
    // check descr size if > 2 throw exception
    if (descr.length() != 2) {
      throw new IllegalArgumentException("Input String longer than 2 expect 2 get " + descr.length());
    }
    
    char rowLetter = descr.toUpperCase().charAt(0);
    if (rowLetter < 'A' || rowLetter > 'Z') {
      throw new IllegalArgumentException("Invalid Row Letter");
    }

    char colLetter = descr.charAt(1);
    if (colLetter < '0' || colLetter > '9') {
      throw new IllegalArgumentException("Invalid Col Letter");
    }

    this.row = rowLetter - 'A';
    this.column = colLetter - '0';
  }

  public int getRow() {
    return this.row;
  }

  public int getColumn() {
    return this.column;
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }

    return false;
  }

  @Override
  public String toString() {
    return "(" + row + ", " + column + ")";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }
}
