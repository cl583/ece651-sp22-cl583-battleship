package edu.duke.cl583.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

  /**
   * Check whether there is collision with this placement
   * @param: theShip is the ship to place
   * @param: theBoard is the board to place
   * @return: true means no collision, false means collision with this placement
   */
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    Iterable<Coordinate> coords = theShip.getCoordinates();
    for (Coordinate c : coords) {
      if (theBoard.whatIsAtForSelf(c) != null) {
        return "That placement is invalid: the ship overlaps another ship.";
      }
    }

    return null;
  }

  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
  
}
