package edu.duke.cl583.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
  /** 
   * Function to make occupy coordinate for a ship based on width and height
   * @param upperLeft is the first position to place ship
   * @param width is the width of ship
   * @param heigt is the height of ship
   * @return the HashSet that the coordinate that ship occupies
   */

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
    HashSet<Coordinate> ret = new HashSet<Coordinate>();

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        int row = upperLeft.getRow() + i;
        int col = upperLeft.getColumn() + j;
        Coordinate c = new Coordinate(row, col);
        ret.add(c);
      }
    }

    return ret;
  }

  /** 
   * Constructor for RectangleShip
   * @param name is the name for ship
   * @param upperLeft is the first position to place ship
   * @param width is the width of ship
   * @param heigt is the height of ship
   * @param toDisplay is the view of own Board
   * @param enemyDisplay is the view of enemyBoard
   */
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, SimpleShipDisplayInfo<T> toDisplay, SimpleShipDisplayInfo<T> enemyDisplay) {
    super(makeCoords(upperLeft, width, height), toDisplay, enemyDisplay, name);
  }

  /** 
   * Constructor for RectangleShip
   * @param name is the name for ship
   * @param upperLeft is the first position to place ship
   * @param width is the width of ship
   * @param heigt is the height of ship
   * @param data is the view to display for a ship
   * @param onHit is the view to display if ship get hit
   */
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
  }

  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testship", upperLeft, 1, 1, data, onHit);
  }

}
