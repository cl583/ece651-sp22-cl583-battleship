package edu.duke.cl583.battleship;
/**
 * BattleShipBoard provide method for moving and manipulate
 * battleship in battleship game
 * @author: Ian Liu
 * @version: 1.0
 * @since: 2022-01-23
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
  private final int width;
  private final int height;
  ArrayList<Ship<T>> myShips;
  private final PlacementRuleChecker<T> placementRuleChecker;
  HashSet<Coordinate> enemyMiss;
  final T missInfo;

  @Override
  public int getHeight() {
    return height;
  }

  @Override
  public int getWidth() {
    return width;
  }

  /**
   * Constructs somthing
   * @param w is the width of newly contructed board.
   * @param h is the height of newly contructed board.
   * @throws IllegalArgumentException if the width or height are less than or equal to zero.
   */

  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null)), missInfo);
  }

  /**
   * Constructs somthing
   * @param w is the width of newly contructed board.
   * @param h is the height of newly contructed board.
   * @param placementRuleChecker rule cheker
   * @param missInfo info to show when missIno
   * @throws IllegalArgumentException if the width or height are less than or equal to zero.
   */
  public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementRuleChecker, T missInfo) {
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
    }

    if (h <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
    }
    
    this.height = h;
    this.width = w;
    this.myShips = new ArrayList<Ship<T>>();
    this.placementRuleChecker = placementRuleChecker;
    this.enemyMiss = new HashSet<Coordinate>();
    this.missInfo = missInfo;
  }

  /**
   * add ship to board
   * @param toAdd: the ship to add to board
   */
  public String tryAddShip(Ship<T> toAdd) {
    if (placementRuleChecker.checkPlacement(toAdd, this) != null) {
      return placementRuleChecker.checkPlacement(toAdd, this);
    }
    
    myShips.add(toAdd);
    return null;
  }

  /**
   * Information at certain coordinate for self
   * @param where: the coordinate to check
   * @return the information in coordinate where
   */
  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  /**
   * fire at ship at position c
   * @param c: coordinate to attack
   * @return ship get attacker
   */
  public Ship<T> fireAt(Coordinate c) throws IOException {
    for (Ship<T> ship : myShips) {
      if (c.getRow()  >= this.height || c.getRow() < 0 || c.getColumn() >= this.width || c.getColumn() < 0) {
        throw new IOException();
      }
      if (ship.occupiesCoordinates(c) == true) {
        ship.recordHitAt(c);
        return ship;
      }
    }

    enemyMiss.add(c);
    return null;
  }

  /**
   * Check what inforrmation is at location coordinate
   * @param where: coordinate to check
   * @param isSelf: true check its own board, false check enemy board
   * @return information at the Coordinate where
   **/

  protected T whatIsAt(Coordinate where, boolean isSelf) {
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(where)){
        return s.getDisplayInfoAt(where, isSelf);
      }
    }

    if (isSelf == false && enemyMiss.contains(where)) {
      return missInfo;
    }
    
    return null;
  }

  /**
   * Information at certain coordinate for enemy to look
   * @param where: the coordinate to check
   * @return the information in coordinate where
   */
  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }
  
  @Override
  public boolean isAllSunk() {
    for (Ship<T> ship : myShips) {
      if (ship.isSunk() == false) {
        return false;
      }
    }

    return true;
  }

  /** Workable Sonar System
   * @param c: cneter to locate
   * @param enemyBoard: board to detect
   **/
  @Override
  public String doSonar(Coordinate c, Board<T> enemyBoard) {
    String ret = null;
    int submarineCount = 0, carrierCount = 0, destroyCount = 0, battleShipCount = 0;

    int row = c.getRow();
    int col = c.getColumn();

    for (int i = row - 3; i < row + 4; i++) {
      for (int j = row - 3; j < col + 4; j++) {
        if (Math.abs(i - c.getRow()) + Math.abs(j - c.getColumn()) <= 3) {
          Coordinate check = new Coordinate(row + i, col + j);
          for (Ship<T> ship : myShips) {
            if (ship.occupiesCoordinates(check)) {
              if (ship.getName() == "Submarine") {
                submarineCount += 1;
              }

              else if (ship.getName() == "Destroyer") {
                destroyCount += 1;
              }

              else if (ship.getName() == "BattleShip") {
                battleShipCount += 1;
              }

              else if (ship.getName() == "Carrier") {
                carrierCount += 1;
              }
            }
          }
        }
      }
    }

    ret = "Submarines occupy " + submarineCount + "\n" + "Destroyers occupy" + destroyCount + "\nBattleships occupy"+
      battleShipCount + "\nCarriers occupy" + carrierCount;
    
    return ret;
  }
  
}
