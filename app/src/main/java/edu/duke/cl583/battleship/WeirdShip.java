package edu.duke.cl583.battleship;

import java.util.HashSet;

public class WeirdShip<T> extends BasicShip<T> {
  /**
   * Function to make occupy coordinate for a ship based on width and height
   * 
   * @param upperLeft is the first position to place ship
   * @param width     is the width of ship
   * @param heigt     is the height of ship
   * @return the HashSet that the coordinate that ship occupies
   */

  static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
    HashSet<Coordinate> ret = new HashSet<Coordinate>();
    int row = upperLeft.getRow();
    int col = upperLeft.getColumn();
    for (int i = 0; i < height - 1; i++) {
      Coordinate c = new Coordinate(row + i, col);
      ret.add(c);
    }

    for (int i = 2; i < height; i++) {
      Coordinate c = new Coordinate(row + i, col + 1);
      ret.add(c);
    }

    return ret;
  }

  /**
   * Constructor for WeirdShip
   * 
   * @param name         is the name for ship
   * @param upperLeft    is the first position to place ship
   * @param width        is the width of ship
   * @param heigt        is the height of ship
   * @param toDisplay    is the view of own Board
   * @param enemyDisplay is the view of enemyBoard
   */
  public WeirdShip(String name, Coordinate upperLeft, int width, int height, SimpleShipDisplayInfo<T> toDisplay,
      SimpleShipDisplayInfo<T> enemyDisplay) {
    super(makeCoords(upperLeft, width, height), toDisplay, enemyDisplay, name);
  }

  /**
   * Constructor for WeirdShip
   * 
   * @param name      is the name for ship
   * @param upperLeft is the first position to place ship
   * @param width     is the width of ship
   * @param heigt     is the height of ship
   * @param data      is the view to display for a ship
   * @param onHit     is the view to display if ship get hit
   */
  public WeirdShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
    this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
        new SimpleShipDisplayInfo<T>(null, data));
  }
}
