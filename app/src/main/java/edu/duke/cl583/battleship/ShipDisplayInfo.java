package edu.duke.cl583.battleship;

public interface ShipDisplayInfo<T> {
  /**
   * Get information at the position of where based on hit or not
   * @param where coordinate to view
   * @param hit indicate the coordinate status
   * @return view information of where based on hit
   **/
  public T getInfo(Coordinate where, boolean hit);
}
