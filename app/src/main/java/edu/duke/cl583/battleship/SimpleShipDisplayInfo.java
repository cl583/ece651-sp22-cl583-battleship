package edu.duke.cl583.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
  private T myData;
  private T onHit;

  public SimpleShipDisplayInfo(T myData, T onHit) {
    this.myData = myData;
    this.onHit = onHit;
  }

  /**
   * Get the information in the Coordinate where
   * 
   * @params where: the Coordinate to check
   * @params: the coordinate condition hit or not
   * @return: the view of this coordinate based on hit or not hit
   */
  public T getInfo(Coordinate where, boolean hit) {
    if (hit == true) {
      return onHit;
    }

    return myData;
  }
}
