package edu.duke.cl583.battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 * @author: Ian Liu
 * @version: 1.0
 * @since: 2021/01/23
 */
public class BoardTextView {
  /**
   * The Board to display
   */
  private final Board<Character> toDisplay;
  /**
   * Constructs a BoardView, given the board it will display.
   * @param toDisplay is the Board to display
   * @throws IllegalArgumentException if the board is larger than 10x26.
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
          "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
    }
  }

  /**
   * display any board based on the passed in lamaba function
   * @param getSquarefn: Information to pass
   * @return the information to display
   **/
  protected String displayAnyBoard(Function<Coordinate, Character> getSquarefn) {
    int rows = toDisplay.getHeight(), colums = toDisplay.getWidth();
    char symbol = 'A';
    String header = this.makeHeader();
    StringBuilder ans = new StringBuilder(header);
    
    for (int row = 0; row < rows; row++) {
      ans.append(symbol);
      String sep = " ";
      for (int column = 0; column < colums; column++) {
        Coordinate c = new Coordinate(row, column);
        ans.append(sep);
        if (getSquarefn.apply(c) != null) {
          ans.append(getSquarefn.apply(c));
        }

        else {
          ans.append(" ");
        }
        sep = "|";
      }
      sep = " ";
      ans.append(sep);
      ans.append(symbol);
      ans.append("\n");
      symbol += 1;
    }
    
    ans.append(header);
    return ans.toString();
  }

  /**
   * Disply enemy's own board
   * @return the string that represent enemy's own board
   */
  public String displayMyOwnBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
  }

  public String displayEnemyBoard() {
    return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
  }

   /**
   * This makes the header line, e.g. 0|1|2|3|4\n
   * 
   * @return the String that is the header line for the given board
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
    String sep=""; //start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth(); i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }

  /**
   * Display board side by side with own board and enemy board
   * @param: enemyView is view interface of enemy board
   * @param: myHeader is the header to show in own board
   * @param: enemyHeader is the header to show in enemy board
   * @return String of two board side by side
   **/
  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    StringBuilder sb = new StringBuilder();
    // calculate space
    int width = toDisplay.getWidth();
    String headerStartSpace = spaceGenerator(5);
    String bodySpace = spaceGenerator(16);
    String headerSpace = spaceGenerator(2 * width + 22 - myHeader.length() - 5); // 5 is header start space
    // add two header line (own start at 5, enemy start at 2*W + 22
    sb.append(headerStartSpace);
    sb.append(myHeader);
    sb.append(headerSpace);
    sb.append(enemyHeader);
    sb.append("\n");
    // add board view (own start at 0, enemy start at 2 * W + 19
    String ownBoard = displayMyOwnBoard();
    String enemyBoard = enemyView.displayEnemyBoard();
    String [] ownBoardList = ownBoard.split("\n");
    String [] enemyBoardList = enemyBoard.split("\n");
    for (int i = 0; i < ownBoardList.length; i++) {
      sb.append(ownBoardList[i]);
      sb.append(bodySpace);
      if (i == 0 || i == ownBoardList.length - 1) {
        sb.append(spaceGenerator(2)); // 2 is for the becuase makeHeader would create header like 'something\n; hence, it miss two space
      }
      sb.append(enemyBoardList[i]);
      sb.append("\n");
    }
    return sb.toString();
  }

  private String spaceGenerator(int width) {
    StringBuilder space = new StringBuilder();
    for (int i = 0; i < width; i++) {
      space.append(" ");
    }

    return space.toString();
  }
  
}
