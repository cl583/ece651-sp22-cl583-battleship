package edu.duke.cl583.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
  /**
   * Checker whether the placement is within the frame of board
   * @param theShip: ship to place
   * @param theBoard: board to be place
   * @return true means within the bound of board, false means outside the board
   */
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    Iterable<Coordinate> Coordinates = theShip.getCoordinates();
    for (Coordinate c : Coordinates) {
      if (c.getRow() < 0) {
        return "That placement is invalid: the ship goes off the top of the board.";
      }

      if (c.getRow() >= theBoard.getHeight()) {
        return "That placement is invalid: the ship goes off the bottom of the board.";
      }

      if (c.getColumn() < 0) {
        return "That placement is invalid: the ship goes off the left of the board.";
      }

      if (c.getColumn() >= theBoard.getWidth()) {
        return "That placement is invalid: the ship goes off the right of the board.";
      }
    }

    return null;
  }

  public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }

}
