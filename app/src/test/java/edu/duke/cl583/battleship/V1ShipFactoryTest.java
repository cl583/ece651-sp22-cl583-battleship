package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
  @Test
  public void test_createShip() {
    Placement p = new Placement("A0H");
    V1ShipFactory vf = new V1ShipFactory();
    Ship<Character> ship = vf.createShip(p, 1, 3, 's', "testShip");
    checkShip(ship, "testShip", 's', new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(0,2)); 
  }

  @Test
  public void test_ships() {
    Placement ph = new Placement("A0H");
    Placement pv = new Placement("B3V");
    V1ShipFactory vf = new V1ShipFactory();

    Ship<Character> submarine = vf.makeSubmarine(ph);
    checkShip(submarine, "Submarine", 's', new Coordinate(0, 0), new Coordinate(0, 1));

    Ship<Character> battleShip = vf.makeBattleship(pv);
    checkShip(battleShip, "Battleship", 'b', new Coordinate(1, 3), new Coordinate(2, 3), new Coordinate(3, 3), new Coordinate(4, 3));

    Ship<Character> destroyer = vf.makeDestroyer(pv);
    checkShip(destroyer, "Destroyer", 'd', new Coordinate(1, 3), new Coordinate(2, 3), new Coordinate(3, 3));

    Ship<Character> carrier = vf.makeCarrier(pv);
    checkShip(carrier, "Carrier", 'c', new Coordinate(1, 3), new Coordinate(2, 3), new Coordinate(3, 3), new Coordinate(4, 3), new Coordinate(5, 3), new Coordinate(6, 3));
  }

  private void checkShip(Ship<Character> testShip, String expectedName,
                         char expectedLetter, Coordinate... expectedLocs) {
    assertEquals(expectedName, testShip.getName());
    for (Coordinate c : expectedLocs) {
      assertEquals(testShip.occupiesCoordinates(c), true);
      assertEquals(testShip.getDisplayInfoAt(c, true), expectedLetter);
    }
  }

}
