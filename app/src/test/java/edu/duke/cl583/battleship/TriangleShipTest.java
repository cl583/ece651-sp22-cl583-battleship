package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class TriangleShipTest {
  @Test
  public void test_buildTriangleShip() {
    Coordinate c = new Coordinate("D3");
    Ship<Character> ship = new TriangleShip<Character>("testShip", c, 3, 2, 'b', '*');
    Iterable<Coordinate> coords = ship.getCoordinates();
    HashSet<Coordinate> expected = new HashSet<Coordinate>();
    Coordinate  c1 = new Coordinate(0, 0);
    expected.add(new Coordinate(c.getRow(), c.getColumn() + 1));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn()));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn() + 1));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn() + 2));
    
    for (Coordinate coord : coords) {
      assertTrue(expected.contains(coord));
    }
  }

  @Test
  public void test_destoryCoordinate() {
    Coordinate c = new Coordinate("D3");
    Ship<Character> ship = new TriangleShip<Character>("testShip", c, 3, 2, 'b', '*');
    ship.destroyCoordinate();
    Iterable<Coordinate> coords = ship.getCoordinates();
    int count = 0;
    for (Coordinate coord : coords) {
      count += 1;
    }
    assertEquals(0, count);
  }

  @Test
  public void test_addCoordinate() {
    Coordinate c = new Coordinate("A0");
    Ship<Character> ship = new TriangleShip<Character>("testShip", c, 3, 2, 'b', '*');
    
    HashSet<Coordinate> exp = new HashSet<Coordinate>();
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 3; j++) {
        Coordinate newCoord = new Coordinate(c.getRow() + i, c.getColumn() + j);
        exp.add(newCoord);
        ship.addCoordinate(newCoord);
      }
    }
    
    Iterable<Coordinate> coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      assertEquals(true, exp.contains(coord));
    }
  }

}
