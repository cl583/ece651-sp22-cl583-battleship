package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
  
  @Test
  public void test_triangleShip() {
    // test up
    
    V2ShipFactory vf = new V2ShipFactory();
    /*
    Placement p = new Placement("D3U");
    Ship<Character> ship = vf.makeBattleship(p);
    Coordinate c = p.getCoordinate();
    HashSet<Coordinate> expected = new HashSet<Coordinate>();
    expected.add(new Coordinate(c.getRow(), c.getColumn() + 1));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn()));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn() + 1));
    expected.add(new Coordinate(c.getRow() + 1, c.getColumn() + 2));

    for (Coordinate coord : ship.getCoordinates()) {
      assertTrue(expected.contains(coord));
    }
    */
    
    // test down
    Placement p2 = new Placement("D3D");
    Ship<Character> ship2 = vf.makeBattleship(p2);
    HashSet<Coordinate> e2 = new HashSet<Coordinate>();
    int r2 = p2.getCoordinate().getRow();
    int c2 = p2.getCoordinate().getColumn();
    e2.add(new Coordinate(r2 + 1, c2 + 1));
    for (int i = 0; i < 3; i++) {
      e2.add(new Coordinate(r2, c2 + i));
    }

    for (Coordinate coord : ship2.getCoordinates()) {
      System.out.println(coord);
      assertTrue(e2.contains(coord));
    }

    // assertEquals(true, false);

    
    // test left
    Placement p3 = new Placement("D3L");
    Ship<Character> ship3 = vf.makeBattleship(p3);
    HashSet<Coordinate> e3 = new HashSet<Coordinate>();
    int r3 = p3.getCoordinate().getRow();
    int c3 = p3.getCoordinate().getColumn();
    e3.add(new Coordinate(r3 + 1, c2));
    for (int i = 0; i < 3; i++) {
      e3.add(new Coordinate(r3 + i, c3 + 1));
    }

    for (Coordinate coord : ship3.getCoordinates()) {
      assertTrue(e3.contains(coord));
    }
    
    // test right
    Placement p4 = new Placement("D3R");
    Ship<Character> ship4 = vf.makeBattleship(p4);
    HashSet<Coordinate> e4 = new HashSet<Coordinate>();
    int r4 = p4.getCoordinate().getRow();
    int c4 = p4.getCoordinate().getColumn();
    e4.add(new Coordinate(r4 + 1, c4 + 1));
    for (int i = 0; i < 3; i++) {
      e4.add(new Coordinate(r4 + i, c4));
    }

    for (Coordinate coord : ship4.getCoordinates()) {
      assertTrue(e4.contains(coord));
    }
    
  }

  @Test
  public void test_create2DMatrix() {
    Coordinate c = new Coordinate("A0");
    int triWidth = 3, triHeight = 2;
    Ship<Character> ship = new TriangleShip<Character>("testship", c, triWidth, triHeight, 'x', 'x');
    V2ShipFactory vf = new V2ShipFactory();
    Character matrix[][] = vf.create2DMatrix(c, ship, triWidth, triHeight);
    Iterable<Coordinate> coords = ship.getCoordinates();

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + ", ");
      }
      System.out.print('\n');
    }

    System.out.print("\n");
    vf.rotateMatrix90Deg(matrix);
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + ", ");
      }
      System.out.print('\n');
    }

    System.out.print("\n");
    vf.rotateMatrix90Deg(matrix);
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + ", ");
      }
      System.out.print('\n');
    }

    System.out.print("\n");
    vf.rotateMatrix90Deg(matrix);
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + ", ");
      }
      System.out.print('\n');
    }

    System.out.print("\n");
    vf.rotateMatrix90Deg(matrix);
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        System.out.print(matrix[i][j] + ", ");
      }
      System.out.print('\n');
    }

    // assertEquals(true, false);
  }

  @Test
  public void test_alignCoordinateWUpperRight() {
    // create a triangleShip
    Placement p = new Placement("C2U");
    V2ShipFactory vf = new V2ShipFactory();
    Ship<Character> ship = vf.createTriangleShip(p, 3, 2, 'b', "testship");
    HashSet<Coordinate> exp = new HashSet<Coordinate>();
    int row = p.getCoordinate().getRow();
    int col = p.getCoordinate().getColumn();
    Coordinate top = new Coordinate(row, col + 1);
    exp.add(top);
    for (int i = 0; i < 3; i++) {
      Coordinate coord = new Coordinate(row + 1, col + i);
      exp.add(coord);
    }

    Iterable<Coordinate> coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      assertEquals(true, exp.contains(coord));
    }

    
    // make a 90 degree rotation
    vf.rotateClockWise90Deg(p.getCoordinate(), ship, 3, 2);
    exp.clear();
    top = new Coordinate(p.getCoordinate().getRow() + 1, p.getCoordinate().getColumn() + 1);
    exp.add(top);
    for (int i = 0; i < 3; i++) {
      Coordinate coord = new Coordinate(row + i, col);
      exp.add(coord);
    }

    coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      assertEquals(true, exp.contains(coord));
    }

    // rotate 180
    vf.rotateClockWise90Deg(p.getCoordinate(), ship, 3, 2);
    exp.clear();
    top = new Coordinate(p.getCoordinate().getRow() + 1, p.getCoordinate().getColumn() + 1);
    exp.add(top);
    for (int i = 0; i < 3; i++) {
      Coordinate coord = new Coordinate(row, col + i);
      exp.add(coord);
    }

    coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      // System.out.println(coord);
      assertEquals(true, exp.contains(coord));
    }

    // rotate 270
    vf.rotateClockWise90Deg(p.getCoordinate(), ship, 3, 2);
    exp.clear();
    top = new Coordinate(p.getCoordinate().getRow() + 1, p.getCoordinate().getColumn());
    exp.add(top);
    for (int i = 0; i < 3; i++) {
      Coordinate coord = new Coordinate(row + i, col + 1);
      exp.add(coord);
    }

    coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      // System.out.println(coord);
      assertEquals(true, exp.contains(coord));
    }

    // rotate 360
    vf.rotateClockWise90Deg(p.getCoordinate(), ship, 3, 2);
    exp.clear();
    top = new Coordinate(p.getCoordinate().getRow(), p.getCoordinate().getColumn() + 1);
    exp.add(top);
    for (int i = 0; i < 3; i++) {
      Coordinate coord = new Coordinate(row + 1, col + i);
      exp.add(coord);
    }

    coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      //System.out.println(coord);
      assertEquals(true, exp.contains(coord));
    }

    // assertEquals(true, false);
  }

  @Test
  public void test_checkOffUpper() {
    Character[][] board = { { 'E', 'E', 'E' }, { 'O', 'O', 'O' }, { 'E', 'E', 'O' }, };

    V2ShipFactory vf = new V2ShipFactory();
    assertEquals(-1, vf.checkOffUpper(board));

    Character[][] board2 = { { 'E', 'O', 'E' }, { 'E', 'O', 'O' }, { 'E', 'O', 'E' }, };

    assertEquals(-2, vf.checkOffUpper(board2));

    Character[][] board3 = { { 'E', 'O', 'E' }, { 'O', 'O', 'O' }, { 'E', 'E', 'E' }, };
    assertEquals(0, vf.checkOffUpper(board3));

    Character [][] board4 = {
      {'E', 'E', 'E', 'O', 'E'},
      {'E', 'E', 'E', 'O', 'O'},
      {'E', 'E', 'E', 'O', 'O'},
      {'E', 'E', 'E', 'E', 'O'},
      {'E', 'E', 'E', 'E', 'O'},
    };

    assertEquals(-2, vf.checkOffUpper(board4));

    Character [][] board5 = {
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'O', 'O', 'O'},
      {'O', 'O', 'O', 'O', 'E'},
    };

    assertEquals(-1, vf.checkOffUpper(board5));
  }

  @Test
  void test_addNewCoordinate() {
    Placement p = new Placement("C3H");
    V2ShipFactory vf = new V2ShipFactory();
    Ship<Character> ship = vf.createTriangleShip(p, 3, 2, 'b', "testship");
    Character matrix[][] = {
      {'E', 'O', 'E'},
      {'O', 'O', 'E'},
      {'E', 'O', 'E'},
    };
    
    vf.addNewCoordinate(ship, p.getCoordinate(), matrix);
    HashSet<Coordinate> exp = new HashSet<Coordinate>();
    exp.add(new Coordinate(3, 3));
    exp.add(new Coordinate(2, 4));
    exp.add(new Coordinate(4, 4));
    exp.add(new Coordinate(3, 4));

    Iterable<Coordinate> coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      // System.out.println(coord);
      assertEquals(true, exp.contains(coord));
    }

  }


  @Test
  void test_alignCoordinateWUpperLeft() {
    Character[][] board = { { 'E', 'E', 'E' }, { 'O', 'O', 'O' }, { 'E', 'O', 'E' }, };
    Character[][] expectedBoard = { { 'O', 'O', 'O' }, { 'E', 'O', 'E' }, { 'E', 'E', 'E' }, };

    V2ShipFactory vf = new V2ShipFactory();
    vf.alignCoordinateWUpperLeft(board);
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[i].length; j++) {
        assertEquals(expectedBoard[i][j], board[i][j]);
      }
    }

    Character[][] board2 = { { 'E', 'O', 'E' },
                             { 'E', 'O', 'O' },
                             { 'E', 'O', 'E' }, };
    Character[][] expectedBoard2 = { { 'O', 'E', 'E' },
                                     { 'O', 'O', 'E' },
                                     { 'O', 'E', 'E' }, };

    vf.alignCoordinateWUpperLeft(board2);
    for (int i = 0; i < board2.length; i++) {
      for (int j = 0; j < board2[i].length; j++) {
        assertEquals(expectedBoard2[i][j], board2[i][j]);
      }
    }

    Character [][] board3 = {
      {'E', 'E', 'E', 'O', 'E'},
      {'E', 'E', 'E', 'O', 'O'},
      {'E', 'E', 'E', 'O', 'O'},
      {'E', 'E', 'E', 'E', 'O'},
      {'E', 'E', 'E', 'E', 'O'},
    };

    Character[][] expectedBoard3 =
    {
      {'O', 'E', 'E', 'E', 'E'},
      {'O', 'O', 'E', 'E', 'E'},
      {'O', 'O', 'E', 'E', 'E'},
      {'E', 'O', 'E', 'E', 'E'},
      {'E', 'O', 'E', 'E', 'E'},
    };

    vf.alignCoordinateWUpperLeft(board3);
    for (int i = 0; i < board3.length; i++) {
      for (int j = 0; j < board3[i].length; j++) {
        assertEquals(expectedBoard3[i][j], board3[i][j]);
      }
    }

    Character [][] board4 = {
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'O', 'O', 'O'},
      {'O', 'O', 'O', 'O', 'E'},
    };

    Character [][] expectedBoard4 = {
      {'E', 'E', 'O', 'O', 'O'},
      {'O', 'O', 'O', 'O', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
      {'E', 'E', 'E', 'E', 'E'},
    };

    vf.alignCoordinateWUpperLeft(board4);
    for (int i = 0; i < board4.length; i++) {
      for (int j = 0; j < board4[i].length; j++) {
        assertEquals(expectedBoard4[i][j], board4[i][j]);
      }
    }
  }

}
