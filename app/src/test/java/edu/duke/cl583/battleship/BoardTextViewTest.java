package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }
  
  @Test
  public void test_disply_empty_2by2() {
    String expectedBody =
      "A  |  A\n"+
      "B  |  B\n";
    String expectedHeader = "  0|1\n";
    emptyBoardHelper(2,2, expectedHeader, expectedBody);
  }

  
  @Test
  public void test_disply_empty_3by2() {
    String expectedBody =
      "A  |  A\n"+
      "B  |  B\n"+
      "C  |  C\n";
    String expectedHeader = "  0|1\n";
    emptyBoardHelper(2, 3, expectedHeader, expectedBody);
  }

  @Test
  public void test_disply_empty_3by5() {
    String expectedBody =
      "A  | | | |  A\n"+
      "B  | | | |  B\n"+
      "C  | | | |  C\n";
    String expectedHeader = "  0|1|2|3|4\n";
    emptyBoardHelper(5, 3, expectedHeader, expectedBody);
  }
  
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  @Test
  public void test_board_with_ships_4by5() {
    Board<Character> board = new BattleShipBoard<Character>(5, 4, 'X');
    Coordinate c1 = new Coordinate(0, 4);
    RectangleShip<Character> s = new RectangleShip<Character>(c1, 's', '*');

    Coordinate c2 = new Coordinate(2, 0);
    RectangleShip<Character> s2 = new RectangleShip<Character>(c2, 's', '*');

    Coordinate c3 = new Coordinate(3, 3);
    RectangleShip<Character> s3 = new RectangleShip<Character>(c3, 's', '*');

    board.tryAddShip(s);
    board.tryAddShip(s2);
    board.tryAddShip(s3);

    String expectedBody =
      "A  | | | |s A\n"+
      "B  | | | |  B\n"+
      "C s| | | |  C\n"+
      "D  | | |s|  D\n";
    String expectedHeader = "  0|1|2|3|4\n";

    BoardHelper(board, expectedHeader, expectedBody, true);
  }

  private void BoardHelper(Board<Character> board, String expectedHeader, String expectedBody, boolean isSelf) {
    BoardTextView view = new BoardTextView(board);
    assertEquals(view.makeHeader(), expectedHeader);
    String expected = expectedHeader + expectedBody + expectedHeader;
    if (isSelf == true) {
      assertEquals(expected, view.displayMyOwnBoard());
    }

    else {
      assertEquals(expected, view.displayEnemyBoard());
    }
  }

  @Test
  public void test_displayEnemyBoard() throws IOException{
    // add ship in own board (test this because refactring the displayMyOwnBoard)
    String myView =
      "A  | | |  A\n" +
      "B s|s| |  B\n" +
      "C  | | |  C\n";

    String myheader = "  0|1|2|3\n"; 
    
    Board<Character> board = new BattleShipBoard<Character>(4, 3, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Placement p = new Placement("B0H");
    Ship<Character> sub_1 = vf.makeSubmarine(p);
    board.tryAddShip(sub_1);
    BoardHelper(board, myheader, myView, true);
    // add ship in enemy board
    String enemyView =
      "A  | | |  A\n"+
      "B  | | |X B\n"+
      "C  | | |  C\n";

    board.fireAt(new Coordinate("B3"));
    BoardHelper(board, myheader, enemyView, false);
    
  }

  @Test
  public void test_display_own_enemy_board() throws IOException{
    // check empty board
    Board<Character> board = new BattleShipBoard<Character>(3, 3, 'X');
    BoardTextView bc = new BoardTextView(board);
    Board<Character> enemyBoard = new BattleShipBoard<Character>(3, 3, 'X');
    BoardTextView enemyView = new BoardTextView(enemyBoard);
    checkEmptyTwoBoard(bc, enemyView);
    // check board with ship
    V1ShipFactory vf = new V1ShipFactory();
    Placement p1 = new Placement("A0V");
    board.tryAddShip(vf.makeSubmarine(p1));

    Placement p2 = new Placement("B0H");
    enemyBoard.tryAddShip(vf.makeSubmarine(p2));
    enemyBoard.fireAt(new Coordinate(1, 0));
    enemyBoard.fireAt(new Coordinate(2, 0));
    checkTwoBoardWithShip(bc, enemyView);
    
  }

  private void checkEmptyTwoBoard(BoardTextView bc, BoardTextView enemyBoard) {
    String expectedString =
      "     Your ocean             Player B's ocean\n"+
      "  0|1|2                    0|1|2\n"+
      "A  | |  A                A  | |  A\n"+
      "B  | |  B                B  | |  B\n"+
      "C  | |  C                C  | |  C\n"+
      "  0|1|2                    0|1|2\n";

    //System.out.print("print" + expectedString);
    assertEquals(expectedString, bc.displayMyBoardWithEnemyNextToIt(enemyBoard, "Your ocean", "Player B's ocean"));
  }

  private void checkTwoBoardWithShip(BoardTextView bc, BoardTextView enemyBoard) {
    String expectedString =
      "     Your ocean             Player B's ocean\n"+
      "  0|1|2                    0|1|2\n"+
      "A s| |  A                A  | |  A\n"+
      "B s| |  B                B s| |  B\n"+
      "C  | |  C                C X| |  C\n"+
      "  0|1|2                    0|1|2\n";

    //System.out.print("print" + expectedString);
    assertEquals(expectedString, bc.displayMyBoardWithEnemyNextToIt(enemyBoard, "Your ocean", "Player B's ocean"));
  }

}
