package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class WeirdShipTest {
  @Test
  public void test_cons() {
    Coordinate c = new Coordinate("C3");
    Ship<Character> ship = new WeirdShip<>("testship", c, 5, 2, 'c', '*');
    HashSet<Coordinate> hs = new HashSet<Coordinate>();

    for (int i = 0; i < 4; i++) {
      int row = c.getRow() + i;
      int col = c.getColumn();

      Coordinate cc = new Coordinate(row, col);
      hs.add(cc);
    }

    for (int j = 2; j < 5; j++) {
      int row = c.getRow() + j;
      int col = c.getColumn() + 1;

      Coordinate cc = new Coordinate(row, col);
      hs.add(cc);
    }

    Iterable<Coordinate> coords = ship.getCoordinates();
    for (Coordinate coord : coords) {
      assertEquals(true, hs.contains(coord));
    }
  }

}
