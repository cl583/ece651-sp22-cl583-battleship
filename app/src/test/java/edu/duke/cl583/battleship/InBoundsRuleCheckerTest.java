package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_constr() {
    InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
    V1ShipFactory vf = new V1ShipFactory();
    Placement p = new Placement("A4H");
    Placement p2 = new Placement("B0V");
    Ship<Character> sub = vf.makeSubmarine(p);
    Ship<Character> carrier = vf.makeCarrier(p2);
    Ship<Character> sub2 = vf.makeSubmarine(p2);
    Board<Character> board = new BattleShipBoard<Character>(4,4, 'X');

    Coordinate c_top = new Coordinate(-1, 3); //above the top
    Placement p_top = new Placement(c_top, 'V');
    Coordinate c_left = new Coordinate(1, -3);
    Placement p_left = new Placement(c_left, 'H');

    Ship<Character> sub3 = vf.makeSubmarine(p_left);
    Ship<Character> sub4 = vf.makeSubmarine(p_top);

    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkPlacement(sub, board));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.", checker.checkPlacement(carrier, board));
    assertEquals(null, checker.checkPlacement(sub2, board));
    assertEquals("That placement is invalid: the ship goes off the left of the board.", checker.checkPlacement(sub3, board));
    assertEquals("That placement is invalid: the ship goes off the top of the board.", checker.checkPlacement(sub4, board));

    Placement p_bug = new Placement("D9H");
    Ship<Character> des = vf.makeDestroyer(p_bug);
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkPlacement(des, board));
  }

}
