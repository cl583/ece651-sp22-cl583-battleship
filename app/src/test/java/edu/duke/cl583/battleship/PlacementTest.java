package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_contructor_with_coordinate_and_orientation() {
    Coordinate c = new Coordinate("Z9");
    Placement p1 = new Placement(c, 'V');
    Placement p2 = new Placement(c, 'H');

    assertEquals(p1.getCoordinate(), c);
    assertEquals(p2.getCoordinate(), c);
    assertEquals(p1.getOrientation(), 'V');
    assertEquals(p2.getOrientation(), 'H');
  }

  @Test
  public void test_toString() {
    Placement p1 = new Placement(new Coordinate("A5"), 'H');
    assertEquals("(0, 5), H", p1.toString());
  }

  @Test
  public void test_hashCode() {
    Placement p1 = new Placement(new Coordinate("A5"), 'H');
    Placement p2 = new Placement(new Coordinate("A5"), 'H');
    Placement p3 = new Placement(new Coordinate("A5"), 'V');
    Placement p4 = new Placement(new Coordinate("Z5"), 'H');

    assertEquals(p1.hashCode(), p1.hashCode());
    assertEquals(p1.hashCode(), p2.hashCode());
    assertNotEquals(p1.hashCode(), p3.hashCode());
    assertNotEquals(p1.hashCode(), p4.hashCode());
  }

  @Test
  public void test_equals() {
    Placement p1 = new Placement(new Coordinate("A5"), 'H');
    Placement p2 = new Placement(new Coordinate("A5"), 'h');
    Placement p3 = new Placement(new Coordinate("A5"), 'V');
    Placement p4 = new Placement(new Coordinate("Z5"), 'H');

    assertEquals(p1, p1);
    assertEquals(p1, p2);
    assertNotEquals(p1, p3);
    assertNotEquals(p1, p4);
    assertNotEquals(p1, "A5H");
  }

  @Test
  public void test_constructor_string_valid() {
    Placement p1 = new Placement("A0V");
    Coordinate c = new Coordinate("A0");
    assertEquals(p1.getCoordinate(), c);
    assertEquals(p1.getOrientation(), 'V');
    
    Placement p2 = new Placement("Z9h");
    Coordinate c2 = new Coordinate("Z9");
    assertEquals(p2.getCoordinate(), c2);
    assertEquals(p2.getOrientation(), 'H');
  }

  @Test
  public void test_contructor_string_invalid() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0W"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0["));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A00"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0WZ"));
  }

}
