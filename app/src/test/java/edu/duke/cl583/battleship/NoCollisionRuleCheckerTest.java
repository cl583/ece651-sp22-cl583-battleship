package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_collision_rule_checker() {
    NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<Character>(null);
    Board<Character> board = new BattleShipBoard<Character>(4, 4, 'X');
    Placement p = new Placement("A0V");
    V1ShipFactory vf = new V1ShipFactory();
    Ship<Character> sub = vf.makeSubmarine(p);
    assertEquals(null, checker.checkMyRule(sub, board));

    board.tryAddShip(sub);
    Placement dp = new Placement("A0H");
    Ship<Character> sub2 = vf.makeSubmarine(dp);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(sub2, board));
  }

  @Test
  public void test_chain_of_rule() {
    InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
    NoCollisionRuleChecker<Character> checker_c = new NoCollisionRuleChecker<Character>(checker);

    V1ShipFactory vf = new V1ShipFactory();
    Placement p = new Placement("A4H");
    Board<Character> board = new BattleShipBoard<Character>(4, 4, 'X');
    Ship<Character> sub = vf.makeSubmarine(p);

    // test out of bound
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker_c.checkPlacement(sub, board));

    Placement vp = new Placement("A0V");
    Ship<Character> sub2 = vf.makeSubmarine(vp);
    // test valid
    assertEquals(null, checker_c.checkPlacement(sub2, board));
    board.tryAddShip(sub2);

    // test collide
    Placement dp = new Placement("A0H");
    Ship<Character> sub3 = vf.makeSubmarine(dp);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker_c.checkPlacement(sub3, board));
  }

}
