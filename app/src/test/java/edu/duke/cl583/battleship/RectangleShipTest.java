package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_make_coords() {
    Coordinate c1 = new Coordinate(1, 2);
    HashSet<Coordinate> set = RectangleShip.makeCoords(c1, 1, 3);
    ArrayList<Coordinate> expected = make_coord_data(c1, 1, 3);
    validate_coords_set(expected, set);

    Coordinate c2 = new Coordinate(4, 6);
    HashSet<Coordinate> set2 = RectangleShip.makeCoords(c2, 4, 4);
    ArrayList<Coordinate> exp2 = make_coord_data(c2, 4, 4);
    validate_coords_set(exp2, set2);
  }

  private ArrayList<Coordinate> make_coord_data(Coordinate upperLeft, int width, int height) {
    ArrayList<Coordinate> expected = new ArrayList<Coordinate>();
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        Coordinate e = new Coordinate(upperLeft.getRow() + i, upperLeft.getColumn() + j);
        expected.add(e);
      }
    }

    return expected;
  }

  private HashMap<Coordinate, Boolean> make_coord_data_map(ArrayList<Coordinate> list) {
    HashMap<Coordinate, Boolean> ret = new HashMap<Coordinate, Boolean>();
    for (Coordinate c : list) {
      ret.put(c, false);
    }

    return ret;
  }

  private void validate_coords_set(ArrayList<Coordinate> expected, HashSet<Coordinate> test) {
    for (int i = 0; i < expected.size(); i++) {
      assertEquals(test.contains(expected.get(i)), true);
    }
  }

  private void validate_coords_map(HashMap<Coordinate, Boolean> expected, HashMap<Coordinate, Boolean> rs) {
    assertEquals(expected.size(), rs.size());
    for (Map.Entry<Coordinate, Boolean> test : rs.entrySet()) {
      assertEquals(expected.containsKey(test.getKey()), true);
      assertEquals(test.getValue(), false);
    }
  }

  @Test
  public void test_RectangleShip_constr() {
    Coordinate ul = new Coordinate(3, 4);
    RectangleShip<Character> rc = new RectangleShip<Character>(ul, 's', '*');
    ArrayList<Coordinate> expected = make_coord_data(ul, 1, 1);
    HashMap<Coordinate, Boolean> expected_map = make_coord_data_map(expected);

    // System.out.println(rc.myPieces.size());
    validate_coords_map(expected_map, rc.myPieces);
  }

  @Test
  public void test_checkCoordinate() {
    // TODO: refactor duplication

    Coordinate ul = new Coordinate(3, 4);
    RectangleShip<Character> rc = new RectangleShip<Character>("submarine", ul, 3, 2, 's', '*');
    Coordinate wrongCoordinate = new Coordinate(5, 6);
    assertThrows(IllegalArgumentException.class, () -> rc.checkCoordinateInThisShip(wrongCoordinate));
    assertEquals("submarine", rc.getName());

    RectangleShip<Character> rc2 = new RectangleShip<Character>("attacker", ul, 3, 2, 's', '*');
    Coordinate c = new Coordinate(4, 4);
    assertDoesNotThrow(() -> rc2.checkCoordinateInThisShip(c));
    assertEquals("attacker", rc2.getName());

    RectangleShip<Character> rc3 = new RectangleShip<Character>("BigBoat", ul, 3, 2, 's', '*');
    Coordinate c2 = new Coordinate(3, 6);
    assertDoesNotThrow(() -> rc3.checkCoordinateInThisShip(c2));
    assertEquals("BigBoat", rc3.getName());
  }

  @Test
  public void test_recordHitAt_and_wasHitAt() {
    Coordinate ul = new Coordinate(3, 4);
    RectangleShip<Character> rc = new RectangleShip<Character>("testship", ul, 3, 2, 's', '*');

    ArrayList<Coordinate> ac = new ArrayList<Coordinate>();

    Coordinate validCoordinate = new Coordinate(3, 4);
    rc.recordHitAt(validCoordinate);
    ac.add(validCoordinate);
    checkShipCoordinate(rc, ac);

    Coordinate vc2 = new Coordinate(4, 4);
    rc.recordHitAt(vc2);
    ac.add(vc2);
    checkShipCoordinate(rc, ac);

    Coordinate invalid = new Coordinate(6, 4);
    assertThrows(IllegalArgumentException.class, () -> rc.recordHitAt(invalid));
  }

  private void checkShipCoordinate(RectangleShip<Character> rc, ArrayList<Coordinate> testCoordinates) {
    for (Map.Entry<Coordinate, Boolean> c : rc.myPieces.entrySet()) {
      boolean flag = false;
      for (Coordinate testCoordinate : testCoordinates) {
        if (c.getKey().equals(testCoordinate) == true) {
          assertEquals(true, rc.wasHitAt(testCoordinate));
          flag = true;
          break;
        }
      }

      if (flag == false) {
        assertEquals(false, rc.wasHitAt(c.getKey()));
      }
    }
  }

  @Test
  public void test_isSunk() {
    Coordinate c = new Coordinate(3, 2);
    RectangleShip<Character> rc = new RectangleShip<Character>("testship", c, 2, 2, 's', '*');

    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        Coordinate attack = new Coordinate(c.getRow() + i, c.getColumn() + j);
        rc.recordHitAt(attack);
        if (c.getRow() + i != 4 || c.getColumn() + j != 3) {
          assertEquals(false, rc.isSunk());
        }

        else {
          assertEquals(true, rc.isSunk());
        }
      }
    }
  }

  @Test
  public void test_getDisply() {
    Coordinate c = new Coordinate(3, 2);
    RectangleShip<Character> rc = new RectangleShip<Character>("testship", c, 2, 2, 's', '*');
    Coordinate attack1 = new Coordinate(4, 2);
    rc.recordHitAt(attack1);

    assertEquals('*', rc.getDisplayInfoAt(attack1, true));
    assertEquals('s', rc.getDisplayInfoAt(c, true));
    assertEquals('s', rc.getDisplayInfoAt(attack1, false));
  }
}
