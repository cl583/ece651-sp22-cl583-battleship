package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_getInfo() {
    SimpleShipDisplayInfo <Character> display = new SimpleShipDisplayInfo<Character>('s', 'h');
    Coordinate c = new Coordinate(1,1);
    assertEquals('s', display.getInfo(c, false));
    assertEquals('h', display.getInfo(c, true));
  }

}
