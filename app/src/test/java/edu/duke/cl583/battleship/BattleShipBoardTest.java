package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  @Test
  public void test_add_ship() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(5, 5, 'X');
    checkEmtpyBoard(b1);

    V1ShipFactory vf = new V1ShipFactory();
    Placement p = new Placement("A0H");
    Ship<Character> ship = vf.makeSubmarine(p);
    assertEquals(null, b1.tryAddShip(ship));

    Placement dp = new Placement("A0V");
    Ship<Character> ship2 = vf.makeSubmarine(dp);
    assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(ship2));

    Placement outp = new Placement("A9V");
    Ship<Character> ship3 = vf.makeSubmarine(outp);
    assertEquals("That placement is invalid: the ship goes off the right of the board.", b1.tryAddShip(ship3));
  }

  public <T> void checkEmtpyBoard(Board<T> b1) {
    for (int i = 0; i < b1.getHeight(); i++) {
      for (int j = 0; j < b1.getWidth(); j++) {
        Coordinate c = new Coordinate(i, j);
        assertEquals(b1.whatIsAtForSelf(c), null);
      }
    }
  }

  @Test
  public void test_fireAt() throws IOException {
    BattleShipBoard<Character> board = new BattleShipBoard<Character>(5, 5, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Placement p1 = new Placement("A0H");
    Placement p2 = new Placement("B2V");
    Placement p3 = new Placement("B1V");
    Ship<Character> sub_h = vf.makeSubmarine(p1);
    Ship<Character> des = vf.makeDestroyer(p3);
    Ship<Character> sub_v = vf.makeSubmarine(p2);

    board.tryAddShip(sub_h);
    board.tryAddShip(des);
    board.tryAddShip(sub_v);

    assertSame(board.fireAt(new Coordinate(0, 0)), sub_h);
    assertSame(des, board.fireAt(new Coordinate(1, 1)));
    assertSame(sub_v, board.fireAt(new Coordinate(1, 2)));
    assertSame(null, board.fireAt(new Coordinate(4, 4)));
    Coordinate c = new Coordinate(0, 1);
    board.fireAt(c);
    assertEquals(true, sub_h.isSunk());
  }

  @Test
  public void test_whatIsAtForEnemy() throws IOException {
    BattleShipBoard<Character> board = new BattleShipBoard<Character>(5, 5, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    // place some ship
    Placement p = new Placement("A0H");
    Ship<Character> s = vf.makeSubmarine(p);
    board.tryAddShip(s);
    // fire at certain location with miss and not miss
    board.fireAt(p.getCoordinate());
    // test result
    assertEquals('s', board.whatIsAtForEnemy(p.getCoordinate()));
    Coordinate c = new Coordinate("B1");
    board.fireAt(c);
    assertEquals('X', board.whatIsAtForEnemy(c));
  }

  @Test
  public void test_isAllSunk() throws IOException {
    BattleShipBoard<Character> board = new BattleShipBoard<Character>(5, 5, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Placement p = new Placement("A0H");
    Placement p2 = new Placement("B1V");

    board.tryAddShip(vf.makeSubmarine(p));
    board.tryAddShip(vf.makeSubmarine(p2));

    board.fireAt(new Coordinate(0,0));
    board.fireAt(new Coordinate(0, 1));
    assertEquals(false, board.isAllSunk());
    
    board.fireAt(new Coordinate(1, 1));
    board.fireAt(new Coordinate(2, 1));
    board.isAllSunk();
    assertEquals(true, board.isAllSunk());
  }

}
