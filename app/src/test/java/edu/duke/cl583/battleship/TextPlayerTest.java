package edu.duke.cl583.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.function.Function;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  @Test
  void test_read_placement() throws IOException {

    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

    String prompt = "Where would you like to put your Destroyer?\n";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]);
      assertEquals(prompt + "\n", bytes.toString());
      bytes.reset();
    }

    // test IOException
    TextPlayer player2 = createTextPlayer(10, 20, "A0\nA0H\n", bytes);
    player2.readPlacement(prompt);
    assertEquals("Where would you like to put your Destroyer?\n\nInvalid Placement, please re-enter\n", bytes.toString());

  }
  
  @Test
  void test_do_one_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "D9H\nB1V\n", bytes);
    V1ShipFactory vf = new V1ShipFactory();

    String expectedHeader = "  0|1|2|3\n";
    String expectedBody = "A  | | |  A\n" + "B  |d| |  B\n" + "C  |d| |  C\n" + "D  |d| |  D\n";

    String expected = "Player A where would you like to put your Destroyer?\nThat placement is invalid: the ship goes off the right of the board.\nre-enter a non collision or not our of bound placement\n"
        + expectedHeader + expectedBody + expectedHeader;
    
    player.doOnePlacement("Destroyer", (p) -> vf.makeDestroyer(p));
    assertEquals(expected, bytes.toString());
    bytes.reset();
  }

  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }
  @Disabled
  @Test
  public void test_play_one_turn() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "C2\n87\nA1\n", bytes);
    Board<Character> enemyBoard = new BattleShipBoard<Character>(4, 4, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Ship<Character> ship = vf.makeSubmarine(new Placement("A0H"));
    enemyBoard.tryAddShip(ship);
    player.playOneTurn(enemyBoard, "enemy");
    // assertEquals(null, bytes.toString());
    player.playOneTurn(enemyBoard, "enemy");
    String expectedString = "      Your Ocean              enemy's Ocean\n" + "  0|1|2|3                    0|1|2|3\n"
        + "A  | | |  A                A  | | |  A\n" + "B  | | |  B                B  | | |  B\n"
        + "C  | | |  C                C  | | |  C\n" + "D  | | |  D                D  | | |  D\n"
        + "  0|1|2|3                    0|1|2|3\n\n" + "player A choose coordinate to attack\n" + "You missed!\n"
        + "      Your Ocean              enemy's Ocean\n" + "  0|1|2|3                    0|1|2|3\n"
        + "A  | | |  A                A  | | |  A\n" + "B  | | |  B                B  | | |  B\n"
        + "C  | | |  C                C  | |X|  C\n" + "D  | | |  D                D  | | |  D\n"
        + "  0|1|2|3                    0|1|2|3\n\n" + "player A choose coordinate to attack\n"
        + "Invalid Coordinate re-enter!\n" + "player A choose coordinate to attack\n" + "You hit Submarine!\n";

    assertEquals(expectedString, bytes.toString());
  }

  @Test
  public void test_isAllSunk() throws IOException {
    Board<Character> board = new BattleShipBoard<Character>(4, 4, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Ship<Character> sub_1 = vf.makeSubmarine(new Placement("A0H"));
    Ship<Character> sub_2 = vf.makeSubmarine(new Placement("B1H"));

    board.tryAddShip(sub_2);
    board.tryAddShip(sub_1);

    assertEquals(false, board.isAllSunk());
    board.fireAt(new Coordinate("A0"));
    board.fireAt(new Coordinate("A1"));
    board.fireAt(new Coordinate("B1"));
    board.fireAt(new Coordinate("B2"));
    assertEquals(true, board.isAllSunk());
  }

  @Test
  public void test_isLose() throws IOException {
    Board<Character> board = new BattleShipBoard<Character>(4, 4, 'X');
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "C1V\n", bytes);
    V1ShipFactory vf = new V1ShipFactory();
    player.doOnePlacement("testShip", (p) -> vf.makeSubmarine(p));
    player.theBoard.fireAt(new Coordinate("C1"));
    assertEquals(false, player.isLose());
    player.theBoard.fireAt(new Coordinate("D1"));
    assertEquals(true, player.isLose());
  }
  
  @Test
  public void test_IOExcpetion() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "Z0\nA0\n", bytes);
    Board<Character> enemyBoard = new BattleShipBoard<Character>(4, 4, 'X');
    V1ShipFactory vf = new V1ShipFactory();
    Ship<Character> ship = vf.makeSubmarine(new Placement("A0H"));
    enemyBoard.tryAddShip(ship);
    //player.playOneTurn(enemyBoard, "enemy");
  }

  @Test
  public void test_null_readCoordinate() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "", bytes);

    assertThrows(IOException.class, () -> player.readCoordinate("test"));
  }
  
  @Test
  public void invalid_null_readPlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "", bytes);
    assertThrows(EOFException.class, () -> player.readPlacement("test"));
  }
}
